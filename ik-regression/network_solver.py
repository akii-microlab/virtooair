""" Runs deep learning model IK solver on incoming data from Unity visualizer instance.
Returns the calculated joint rotations via the network."""
import os

# Disable GPU -> leads to errors when run at same time as Unity 3D visualizer
os.environ["CUDA_VISIBLE_DEVICES"] = '-1'

import time
from ik_regression import regression_model
import socket
import settings
import numpy as np

# Load keras model.
model = regression_model.load(settings.MODEL_PATH + settings.MODEL_NAME + '.h5')

# Connect to server
udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
udp_socket.bind((settings.OWN_IP, settings.OWN_PORT))
print("Finished initialization. Wait for incoming network packages.")
while True:
    data = udp_socket.recv(settings.NETWORK_BUFFER_SIZE)

    start_time = time.time()
    # Data package layout:
    # vector_head_left_hand[x,y,z], vector_head_right_hand[x,y,z],
    # r_head[x,y,z,w], r_left_hand[x,y,z,w], r_right_hand[x,y,z,w]
    x = np.fromstring(data, dtype=float, sep=',')
    x = x[settings.INPUT_LEARNING_DATA_RANGE]

    # Model prediction of joint rotations
    y = model.predict([[x]])

    y = y.reshape(len(y), len(y[0, :]) * len(y[0, 0, :]))

    # Conversion and send data to Unity 3D visualizer
    # TODO use binary network package and not string -> byte for better performance
    y_string = np.array2string(y[0, :], precision=8, separator=',', max_line_width=1000000)
    # Return string layout:
    # r_spine1[x,y,z,w], r_neck[x,y,z,w], r_left_shoulder[x,y,z,w], r_left_arm[x,y,z,w], r_left_forearm[x,y,z,w],
    # r_right_shoulder[x,y,z,w], r_right_arm[x,y,z,w], r_right_forearm[x,y,z,w])
    send_bytes = bytearray()
    # Send return values as string and without brackets
    send_bytes.extend(map(ord, y_string[1:-1]))
    udp_socket.sendto(send_bytes, (settings.DESTINATION_IP, settings.DESTINATION_PORT))
