""" Fit keras model using the training data defined in the settings file. """

# Load libraries
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from keras.callbacks import EarlyStopping, ModelCheckpoint

import settings
from ik_regression import regression_model

# Load training data. Split between input (x) and output data (y).
# Training data
path = settings.DATA_PATH + 'trainingData.csv'
data = pd.read_csv(path, index_col=False)
x_train = data.values[:, settings.INPUT_LEARNING_DATA_RANGE]
# Normalize vectors
x_train[:, 0:6] /= settings.MAX_ARM_LENGTH
y_train = data.values[:, settings.OUTPUT_LEARNING_DATA_RANGE]

# Test data
path = settings.DATA_PATH + 'testData.csv'
data = pd.read_csv(path, index_col=False)
x_valid = data.values[:, settings.INPUT_LEARNING_DATA_RANGE]
# normalize vectors
x_valid[:, 0:6] /= settings.MAX_ARM_LENGTH
y_valid = data.values[:, settings.OUTPUT_LEARNING_DATA_RANGE]

# Reshape to quaternion representation
y_train = np.reshape(y_train, (y_train.shape[0], int(y_train.shape[1] / 4), 4))
y_valid = np.reshape(y_valid, (y_valid.shape[0], int(y_valid.shape[1] / 4), 4))

input_neurons = (x_train.shape[1])
output_neurons = (y_train.shape[1] * 4)

# Build keras model
mlp_regressor = regression_model.build_model(settings.HIDDEN_NEURONS, input_neurons, output_neurons)

# Train
early_stopping = EarlyStopping(monitor='val_loss', patience=10)
path = settings.MODEL_PATH + settings.MODEL_NAME + '.h5'
checkpoint = ModelCheckpoint(path, monitor='val_loss', verbose=1, save_best_only=True, mode='min')
train_log = mlp_regressor.fit(x_train, y_train, batch_size=settings.BATCH_SIZE, epochs=10000, verbose=2,
                              callbacks=[checkpoint, early_stopping], validation_data=(x_valid, y_valid))
# Plot
plt.plot(train_log.history["loss"], label="loss")
plt.plot(train_log.history["val_loss"], label="val_loss")
plt.legend()
plt.show()
