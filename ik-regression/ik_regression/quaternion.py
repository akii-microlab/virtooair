""" Quaternion handling. Uses the Unity Euler rotation order zyx"""

import math

from pyquaternion import Quaternion


def dot_product(q_1, q_2):
    dot = q_1[0] * q_2[0] + q_1[1] * q_2[1] + q_1[2] * q_2[2] + q_1[3] * q_2[3]
    return dot


def distance(q_1, q_2):
    q_1 = q_1.normalised
    q_2 = q_2.normalised
    dist = math.degrees(2 * math.acos(math.fabs(dot_product(q_1, q_2))))
    return dist


def calculate_rotation_error(ground_truth, prediction):
    mean_distance = 0
    joints = int(len(ground_truth[0]) / 4)
    for i in range(0, len(ground_truth)):
        gt = ground_truth[i]
        pr = prediction[i]
        for q in range(0, joints):
            q_truth = Quaternion(gt[4 * q + 3], gt[4 * q + 0], gt[4 * q + 1], gt[4 * q + 2])
            q_pred = Quaternion(pr[4 * q + 3], pr[4 * q + 0], pr[4 * q + 1], pr[4 * q + 2])
            diff = distance(q_truth, q_pred)
            mean_distance += diff
    mean_distance /= (len(ground_truth) * joints)
    return mean_distance
