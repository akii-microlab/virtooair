""" Keras model related functionality. """
import keras.backend as K
import tensorflow as tf
from keras.layers import Dense, Reshape, Lambda, regularizers
from keras.models import Sequential
from keras.models import load_model


def build_model(neuron_count, input_dims, output_dims):
    """ Build keras regression model based on inputs.

    Args:
        neuron_count (int): Number of neurons per hidden layer.
        input_dims (int): Number of neurons in input layer.
        output_dims (int): Number of neurons in output layer.

    Returns:
        model: Keras build and compiled model. Ready for training.

    """
    model = Sequential()
    model.add(
        Dense(neuron_count, input_dim=input_dims, kernel_initializer='normal', activation='relu', name="input_layer"))
    model.add(Dense(neuron_count, activation='relu', kernel_initializer='he_uniform'))
    model.add(Dense(neuron_count, activation='relu', kernel_initializer='he_uniform'))
    model.add(Dense(output_dims, name="output_layer"))
    model.add(Reshape((int(output_dims / 4), 4), name="quaternion_reshape_layer"))
    # Add a lambda layer which normalizes an input neuron. Is L2 norm over the 4 dimensions of quaternion.
    model.add(Lambda(_normalize_quaternion, name="normalize_quaternions_layer"))
    model.compile(loss=_rotation_loss_angle, optimizer='Adam', metrics=[_rotation_loss_angle])
    return model


def load(path):
    """ Load pre trained regression model from path.

    Args:
        path (str): Path to keras model weights (.h5 file).

    Returns:
        model: Keras model which can be used for interference.

    """
    return load_model(path, custom_objects={'_normalize_quaternion': _normalize_quaternion,
                                            '_rotation_loss_angle': _rotation_loss_angle,
                                            '_rotation_loss_euklid': _rotation_loss_euclid})


def _normalize_quaternion(x):
    """ Normalize a quaternion in the neural network.

    Args:
        x: Input quaternion

    Returns:
        tf.Tensor: Normalized quaternion.


    """

    return K.l2_normalize(x, axis=K.ndim(x) - 1)


def _rotation_loss_angle(y_true, y_pred):
    """ Equation from: https://link.springer.com/article/10.1007/s10851-009-0161-2
    Rad to Deg = 57.295779513
    (2 * math.acos(math.fabs(dot_product(q_1, q_2)))) * 57.295779513 = 114.591559026 * acos(abs(dot_product(q_1, q_2)))
    The used clipping function prevents NaNs in training.

    Args:
        y_true: Ground truth values (y-values)
        y_pred: Predicted values. Model output.

    Returns:
        tf.Tensor: Mean quaternion angle difference.

    """
    return K.mean(
        K.mean(
            114.591559026 * tf.acos(K.clip(K.abs(K.sum(y_true * y_pred, axis=-1, keepdims=True)), 0.0, 0.999999)),
            axis=0),
        axis=0)


def _rotation_loss_euclid(y_true, y_pred):
    """ Calculate euclidean distance between two rotations (in 4D space).
    Equation from: https://link.springer.com/article/10.1007/s10851-009-0161-2

    Args:
        y_true: Ground truth values (y-values)
        y_pred: Predicted values. Model output.

    Returns:
        tf.Tensor: Mean quaternion Euclidean distance difference.


    """
    # Simplified distance between two quaternions on a hypersphere
    pos = K.sqrt(K.sum(K.square(y_true + y_pred), axis=-1, keepdims=True))
    neg = K.sqrt(K.sum(K.square(y_true - y_pred), axis=-1, keepdims=True))
    return K.mean(K.mean(K.minimum(pos, neg), axis=-1))
