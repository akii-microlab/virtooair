""" Edit settings file and adjust to specific needs. """

# Wrist distance of SMPL in T-pose.
MAX_ARM_LENGTH = 1451
# Path to trained keras model.
MODEL_PATH = 'model/'
# Data folder path containing the training data.
DATA_PATH = 'data/'
# Name of the trained keras model.
MODEL_NAME = 'model'
# Network settings to communicate with Unity 3D visualizer
OWN_IP = "127.0.0.1"
OWN_PORT = 5005
DESTINATION_IP = "127.0.0.1"
DESTINATION_PORT = 5006
NETWORK_BUFFER_SIZE = 1024
# Size of mini-batches for training via keras.
BATCH_SIZE = 32
# Numbers of neurons in hidden layer of regression model.
HIDDEN_NEURONS = 32
# Range of input data from single training data line.
# Remove ground truth hands because they lead to worse performance.
# Full input range:
# INPUT_LEARNING_DATA_RANGE = range(0, 18)
INPUT_LEARNING_DATA_RANGE = range(0, 10)
# Range of training data output (y-data) from single training sample line.
OUTPUT_LEARNING_DATA_RANGE = range(18, 50)
