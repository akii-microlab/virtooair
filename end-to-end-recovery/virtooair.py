"""
VIRTOAIR Demo. Use this code to run the lower-body reconstruction and stream the data to Unity
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import atexit
import sys
import time
from signal import signal, SIGTERM
from sys import exit

import cv2
import numpy as np
import src.config
import tensorflow as tf
from absl import flags
from src.RunModel import RunModel
from src.util import image as img_util
from src.util import renderer as vis_util

from src.virtooair.bounding_box import BoundingBox
from src.virtooair.capture import Capture
from src.virtooair.network import Network

# --- Change global variable settings below ---
# Set to true in order to directly visualize the network output
VISUALIZE = True
# Use a frame buffer to smooth the predictions
BUFFER_SIZE = 1
# The IP address of the PC where this code is running on
OWN_IP = '192.168.1.2'
# The port which shall be used to receive incoming messages.
OWN_PORT = 5011
# The IP address of the PC with the visualizer running on.
DESTINATION_IP = '192.168.1.1'
# The port which the visualizer listens to for new incoming messages.
DESTINATION_PORT = 5010


def preprocess_image(img):
    """ Preprocess input image to be used for the end-to-end recovery framework.
    Take input image and calculate bounding box around user and scale and crop to required format.

    Args:
        img: Input image with user.
    """
    scale, center = bounding_box(img)
    crop, proc_param = img_util.scale_and_crop(img, scale, center, config.img_size)
    # Normalize image to [-1, 1]
    crop = 2 * ((crop / 255.) - 0.5)
    return crop, proc_param, img


def render(img, proc_param, verts, cams, joints):
    """ Render image with SMPL overlay.

    Args:
        img: Input image.
        proc_param: Processing parameters.
        verts: Vertices of SMPL model.
        cams: Camera parameters.
        joints: Joint rotations for SMPL model.
    """
    cam_for_render, vert_shifted, joints_orig = vis_util.get_original(proc_param, verts[0], cams[0], joints[0],
                                                                      img_size=img.shape[:2])
    rend_img_overlay = renderer(vert_shifted, cam=cam_for_render, img=img, do_alpha=True)
    cv2.imshow('Rendered SMPL', rend_img_overlay)


def main():
    """ The main loop. Take the incoming video stream and use the end-to-end recovery framework to estimate the
    joint poses."""
    sess = tf.Session()
    model = RunModel(config, sess=sess)
    frame_buffer = np.ndarray(shape=(BUFFER_SIZE, 72))
    i = 0
    while True:
        start_time = time.time()
        ret, frame = capture()
        if ret is False:
            break

        input_img, proc_param, img = preprocess_image(frame)

        # Add batch dimension: 1 x D x D x 3
        input_img = np.expand_dims(input_img, 0)

        joints, verts, cams, joints3d, theta = model.predict(input_img, get_theta=True)

        # Send pose (theta)
        curr_pose = theta[0, 3:75]
        frame_buffer[i] = curr_pose
        # Normalize if you want to avoid noise but increase latency
        pose = frame_buffer.mean(axis=0)
        streamer.send_pose(pose)

        if VISUALIZE:
            render(img, proc_param, verts, cams, joints)
            if cv2.waitKey(1) & 0xFF == ord('r'):
                print('Recalibrate background image')
                bounding_box.set_background(frame)
        i = (i + 1) % BUFFER_SIZE
        print("FPS: " + str(1 / (time.time() - start_time)))
    capture.release()


def cleanup():
    """ End of reconstruction. Close camera."""
    print('Interrupted. Cleanup capture')
    capture.release()


if __name__ == '__main__':
    config = flags.FLAGS
    config(sys.argv)
    # Using pre-trained model, change this to use your own.
    config.load_path = src.config.PRETRAINED_MODEL
    config.batch_size = 1
    renderer = vis_util.SMPLRenderer(face_path=config.smpl_face_path)
    capture = Capture(camera=0)
    _, f = capture()
    bounding_box = BoundingBox(f, visualize=VISUALIZE)
    # Create network connection to visualizer.
    streamer = Network(OWN_IP, OWN_PORT, DESTINATION_IP, DESTINATION_PORT)
    atexit.register(cleanup)
    # Normal exit when killed
    signal(SIGTERM, lambda signum, stack_frame: exit(1))
    print("Started reconstruction process. Press 'r' key to recalibrate background.")
    main()
