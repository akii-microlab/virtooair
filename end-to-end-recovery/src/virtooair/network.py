import socket


class Network(object):

    def __init__(self, own_ip, own_port, dest_ip, dest_port, buffer_size=2048):
        """ Network connection to visualizer. """
        self.own_ip = own_ip
        self.own_port = own_port
        self.dest_ip = dest_ip
        self.dest_port = dest_port
        self.buffer_size = buffer_size
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.bind((self.own_ip, self.own_port))

    def _send(self, message):
        """ Send new message string to visualizer.

        Args:
            message (str): Message which shall be send to connected client.
        """
        send_bytes = bytearray(message)
        self.socket.sendto(send_bytes, (self.dest_ip, self.dest_port))

    def send_pose(self, pose):
        """ Send new pose update. All pose messages begin with a 'p' character.

        Args:
            pose: The SMPL pose in axis angle representation.
        """
        msg = 'p'
        for p in pose:
            msg += ',' + str(p)
        self._send(msg)

    def receive(self):
        """ Receive new incoming packages """
        self.socket.recv(self.buffer_size)

    def close(self):
        """ Close socket. """
        self.socket.close()
