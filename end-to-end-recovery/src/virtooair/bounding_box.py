""" Background subtraction for foreground mask computation. Used to calcultate a bounding box around the user. """

import cv2
import numpy as np


class BoundingBox(object):

    def __init__(self, background, visualize=False):
        """ Simple pixel subtraction background removal algorithm.

        Args:
            background (Image): OpenCV input image used as background plate.
            visualize (bool): Visualize the reconstruction.
        """
        self.background = self._denoise(background)
        self.visualize = visualize

    def __call__(self, frame):
        """ Run the background subtraction and return the bounding box dimensions and picture detail with human.

        Args:
            frame: Input frame from RGB camera.

        Returns:
            tuple: Containing the following values:
                tuple: Scaling factor for input image.
                tuple: Person center for incoming image.
        """
        denoised = self._denoise(frame)
        foreground = cv2.absdiff(self.background.astype(np.uint8), denoised)
        foreground_gray = cv2.cvtColor(foreground, cv2.COLOR_BGR2GRAY)
        ret, mask = cv2.threshold(foreground_gray, 40, 255, cv2.THRESH_BINARY)

        # Use morph_open to remove color noise of camera (small parts in image will be removed)
        kernel = np.ones((10, 10), np.uint8)
        denoised_mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)

        # Calculate bounding box based on black and white image
        im2, contours, hierarchy = cv2.findContours(denoised_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        x, y = list(), list()
        for cnt in contours:
            # Ignore small areas because the human inside the image is comparable large
            if cv2.contourArea(cnt) < 200:
                continue
            # Calculate corners of bounding box
            leftmost = tuple(cnt[cnt[:, :, 0].argmin()][0])
            rightmost = tuple(cnt[cnt[:, :, 0].argmax()][0])
            topmost = tuple(cnt[cnt[:, :, 1].argmin()][0])
            bottommost = tuple(cnt[cnt[:, :, 1].argmax()][0])
            x.append(leftmost[0])
            x.append(rightmost[0])
            y.append(bottommost[1])
            y.append(topmost[1])
        if len(x) == 0 or len(y) == 0:
            min_point = np.ndarray((2,), buffer=np.array([0, 0]), dtype=np.uint32)
            max_point = np.ndarray((2,), buffer=np.array([len(frame), len(frame)]), dtype=np.uint32)
        else:
            min_point = np.ndarray((2,), buffer=np.array([min(x), min(y)]), dtype=np.uint32)
            max_point = np.ndarray((2,), buffer=np.array([max(x), max(y)]), dtype=np.uint32)

        # Draw bounding box as image overlay
        if self.visualize:
            self._draw_bounding_box(mask, min_point, max_point)

        # Scale and center of person in bounding box
        person_height = np.linalg.norm(max_point - min_point)
        if person_height == 0:
            print('bad!')
            import ipdb
            ipdb.set_trace()
        center = (min_point + max_point) / 2.
        scale = 150. / person_height
        return scale, center

    def set_background(self, frame):
        """ Update the background plate used for the background subraction algorithm.

        Args:
            frame:
        """
        print('Set new bounding box background image.')
        self.background = self._denoise(frame)

    @staticmethod
    def _denoise(frame):
        frame = cv2.medianBlur(frame, 5)
        frame = cv2.GaussianBlur(frame, (5, 5), 0)
        return frame

    @staticmethod
    def _draw_bounding_box(image, min_point, max_point):
        cv2.rectangle(image, (min_point[0], min_point[1]), (max_point[0], max_point[1]), (255, 80, 80), 2)
        cv2.imshow('Bounding Box Mask', image)
