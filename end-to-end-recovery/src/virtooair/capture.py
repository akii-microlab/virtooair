""" Capture image stream from OpenCV camera. """
from os import listdir
from os.path import isfile, join

import cv2
import skimage.io as io


class Capture(object):

    def __init__(self, camera=None, image_path=None):
        """ Video capture using OpenCV library. Uses either a camera or a folder containing the video frames.

        Args:
            camera: Connected OpenCV camera.
            image_path: Path to images on hard drive.
        """
        self.camera = camera
        self.image_path = image_path
        self.images = None
        if self.image_path is not None:
            images = [f for f in listdir(image_path) if isfile(join(image_path, f))]
            images.sort()
            self.images = iter(images)
        if self.camera is not None:
            self.cap = cv2.VideoCapture(self.camera)
            self.cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))
            self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
            self.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
            self.cap.set(cv2.CAP_PROP_FPS, 60)
        if (self.image_path is not None and self.camera is not None) or \
                (self.image_path is None and self.camera is None):
            raise ValueError('Capture must have an image/video path or a camera id set')

    def __call__(self):
        """ Get next frame from either the camera or the next image on disk.

        Returns:
            tuple: Tuple with the following values:
                bool: Ret val of openCV capture. False if something went wrong.
                np.array: New image frame.

        """
        frame = None
        ret = False
        if self.image_path is not None:
            ret = True
            frame = io.imread(self.image_path + self.images.next())
        elif self.camera is not None:
            ret, frame = self.cap.read()
        return ret, frame

    def release(self):
        """ Close all windows and close camera if it was opened. """
        self.cap.release()
        cv2.destroyAllWindows()
