﻿using UnityEngine;

public class QuaternionFunctions {

    /// <summary>
    /// Force quaternion to be unique. Two quaternions can represent the same rotation (q == -q). 
    /// </summary>
    /// <param name="q">Input quaternion.</param>
    /// <returns>Distinct quaternion pushed to one side of the hypersphere.</returns>
	public static Quaternion _ForceToOneSideOfHypersphere(Quaternion q)
	{
		var q_ret = q;
		if(q_ret.w < 0f)
		{
			q_ret.x = -q_ret.x;
			q_ret.y = -q_ret.y;
			q_ret.z = -q_ret.z;
			q_ret.w = -q_ret.w;
		}
		return q_ret;
	}
}
