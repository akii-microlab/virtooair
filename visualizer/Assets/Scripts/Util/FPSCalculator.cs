﻿using System;

/// <summary>
/// Simple tool to calculate the current frames per second rate.
/// </summary>
public class FPSCalculator
{
    private int _fps = 0;
    private int _renderedFrames = 0;
    private DateTime _lastTime;

    // Use this for initialization
    public FPSCalculator()
    {
        _lastTime = DateTime.Now;
    }

    public void Update()
    {
        _renderedFrames++;
        if ((DateTime.Now - _lastTime).TotalSeconds >= 1)
        {
            _fps = _renderedFrames;
            _renderedFrames = 0;
            _lastTime = DateTime.Now;
        }
    }

    /// <summary>
    /// Return current frames per second value.
    /// </summary>
    public int CurrentFps()
    {
        return _fps;
    }
}