﻿using UnityEngine;

/// <summary>
/// Simple Cyclic Coordinate Descent (CCD) IK solver implementation.
/// </summary>
public class SimpleCCD
{
    private float[] _theta;
    private float[] _sinus;
    private float[] _cosine;
    private float _tolerance;
    private int _maxIterations;
    private Transform[] _bones;

    /// <summary>
    /// Create a CCD solver which solves the bone IK chain with a defined tolerance and number of iterations.
    /// </summary>
    /// <param name="bones">The IK chain which shall be solved.</param>
    /// <param name="tolerance">How much tolerance is possible for joint position update between end-effector and target. If close enough -> stop</param>
    /// <param name="maxIterations">Number of solver steps until algorithm stops if tolerance has not yet been reached.</param>
    public SimpleCCD(Transform[] bones, float tolerance, int maxIterations)
    {
        this._tolerance = tolerance;
        this._maxIterations = maxIterations;
        this._bones = bones;
        // Initialize required CCD values for each joint.
        _theta = new float[bones.Length];
        _sinus = new float[bones.Length];
        _cosine = new float[bones.Length];
    }

    /// <summary>
    /// Solve IK chain for a new target position.
    /// </summary>
    /// <param name="targetPos">The target position which shall be reached via the end-effector.</param>
    public void Solve(Vector3 targetPos)
    {
        var iteration = 0;
        var distanceToTarget = Vector3.Distance(_bones[_bones.Length - 1].transform.position, targetPos);
        // Try to solve until distance is within tolerance margin or until number of max iterations is reached.
        while (distanceToTarget > _tolerance && iteration < _maxIterations)
        {
            // from second last joint (end effector going back up to the root
            for (var i = _bones.Length - 2; i >= 0; i--)
            {
                // The vector from the ith joint to the end effector
                var r1 = _bones[_bones.Length - 1].transform.position - _bones[i].transform.position;
                // The vector from the ith joint to the target
                var r2 = targetPos - _bones[i].transform.position;

                // to avoid dividing by tiny numbers
                if (r1.magnitude * r2.magnitude <= 0.001f)
                {
                    // _cosine component will be 1 and _sinus will be 0
                    _cosine[i] = 1;
                    _sinus[i] = 0;
                }
                else
                {
                    // find the components using dot and cross product
                    _cosine[i] = Vector3.Dot(r1, r2) / (r1.magnitude * r2.magnitude);
                    _sinus[i] = (Vector3.Cross(r1, r2)).magnitude / (r1.magnitude * r2.magnitude);
                }

                // The axis of rotation is the unit vector along the cross product 
                var axis = (Vector3.Cross(r1, r2)) / (r1.magnitude * r2.magnitude);
                // find the angle between r1 and r2 (and clamp values of _cosine to avoid errors)
                _theta[i] = Mathf.Acos(Mathf.Max(-1, Mathf.Min(1, _cosine[i])));
                // invert angle if _sinus component is negative
                if (_sinus[i] < 0.0f)
                    _theta[i] = -_theta[i];
                // obtain an angle value between -pi and pi, and then convert to degrees
                _theta[i] = (float) SimpleAngle(_theta[i]) * Mathf.Rad2Deg;
                // rotate the ith joint along the axis by _theta degrees in the world space.
                _bones[i].transform.Rotate(axis, _theta[i], Space.World);
            }

            ++iteration;
        }
    }

    /// <summary>
    /// Convert angle to radians between -pi and pi.
    /// </summary>
    /// <param name="theta">Input angle.</param>
    /// <returns>Rotation clamped between -pi and pi.</returns>
    private static double SimpleAngle(double theta)
    {
        theta = theta % (2.0 * Mathf.PI);
        if (theta < -Mathf.PI)
            theta += 2.0 * Mathf.PI;
        else if (theta > Mathf.PI)
            theta -= 2.0 * Mathf.PI;
        return theta;
    }
}