﻿using System;
using UnityEngine;

/// <summary>
/// DIfferent IK solvers for upper body reconstruction.
/// </summary>
public class UpperBodyIKSolver
{
    public bool UseHeuristikSolverForNetworkSolver { get; set; }
	public NetworkConnection NetworkSolverConnection { get; set; }

    private NetworkSolver _networkSolver { get; set; }
    private UpperBodyIKChains _upperBodyIKChains;
	private SolverType _solverType;
	private SimpleCCD _simpleCCDLeftArm;
	private SimpleCCD _simpleCCDRightArm;
	private SimpleCCD _simpleCCDHead;

	public enum SolverType{
		FABRIK,
		CCD,
		LIMB,
		DLS,
		JT,
		NETWORK
	}

    public class UpperBodyIKChains
    {
        public Transform[] LeftArmIKChain { get; set; }
        public Transform[] RightArmIKChain { get; set; }
		public Transform[] HeadIKChain { get; set; }
		public Transform Root { get; set; }
        public Transform LeftHand { get; set; }
        public Transform RightHand { get; set; }
		public Transform Head { get; set; }
    }

    public void Init(SolverType solverType, UpperBodyIKChains upperBodyIKChains)
    {
        _solverType = solverType;
        _upperBodyIKChains = upperBodyIKChains;
        switch (solverType)
        {
            case (SolverType.FABRIK):
                //TODO 
                throw new Exception("IK Solver type " + solverType.ToString() + " not implemented yet");
                break;
			case (SolverType.CCD):
				_simpleCCDLeftArm = new SimpleCCD(_upperBodyIKChains.LeftArmIKChain, 0.001f, 500);
				_simpleCCDRightArm = new SimpleCCD(_upperBodyIKChains.RightArmIKChain, 0.001f, 500);
				_simpleCCDHead = new SimpleCCD(_upperBodyIKChains.HeadIKChain, 0.001f, 500);
                break;
            case (SolverType.LIMB):
                //TODO 
                throw new Exception("IK Solver type " + solverType.ToString() + " not implemented yet");
                //RootMotion.FinalIK.IKSolverLimb limbSolverLeft = new RootMotion.FinalIK.IKSolverLimb(AvatarIKGoal.LeftHand);
                //limbSolverLeft.bendModifier = RootMotion.FinalIK.IKSolverLimb.BendModifier.Arm;
                //limbSolverLeft.bendNormal = new Vector3(1, 0, 0);
                // Swap y direction for limp solver. Otherwise it's always off
                //_BenchmarkIKChainLeftArm[0].parent.eulerAngles = new Vector3(0, 180, 0);
                break;
            case (SolverType.DLS):
                //TODO 
                throw new Exception("IK Solver type " + solverType.ToString() + " not implemented yet");
                break;
            case (SolverType.JT):
                //TODO 
                throw new Exception("IK Solver type " + solverType.ToString() + " not implemented yet");
                break;
            case (SolverType.NETWORK):
                _networkSolver = new NetworkSolver(_upperBodyIKChains, NetworkSolverConnection, UseHeuristikSolverForNetworkSolver);
                // Rotate to base orientation of joint
                //TODO Still needed?
                //_BenchmarkIKChainLeftArm[0].parent.eulerAngles = new Vector3(0, 90, -90);
                // Set use rotation limits to false because it causes problems if set on
                break;
        }
    }

	public void SetArmDistanceNetworkSolver(float distance)
	{
		_networkSolver.HandDistanceSMPL = distance;
	}

    public void Solve()
    {
        if (_solverType == SolverType.NETWORK)
        {
            _networkSolver.Update();
        }
		else if (_solverType == SolverType.CCD)
		{
			_simpleCCDLeftArm.Solve(_upperBodyIKChains.LeftHand.position);
			_simpleCCDRightArm.Solve (_upperBodyIKChains.RightHand.position);
			_simpleCCDHead.Solve (_upperBodyIKChains.Head.position);
		} else
        {
            //TODO 
            throw new Exception("IK Solver type " + _solverType.ToString() + " not implemented yet");
        }
    }
}
