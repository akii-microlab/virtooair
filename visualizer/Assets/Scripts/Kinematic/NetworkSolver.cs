﻿using UnityEngine;
using System.Linq;

/// <summary>
/// IK solver using a trained model. All incoming packages will be handled here and final IK will be applied.
/// Used to solve upper-body kinematics of SMPL model.
/// </summary>
public class NetworkSolver
{
    public float HandDistanceSMPL { get; set; }

    private UpperBodyIKSolver.UpperBodyIKChains _upperBodyIKChains;
    private Transform _root;
    private SimpleCCD _simpleCCDLeftArm;
    private SimpleCCD _simpleCCDRightArm;
    private SimpleCCD _simpleCCDHead;
    private NetworkConnection _networkConnection;
    private bool _useHeuristikSolver = false;

    public NetworkSolver(UpperBodyIKSolver.UpperBodyIKChains upperBodyIKChains, NetworkConnection networkConnection,
        bool useHeuristikSolver = true)
    {
        _upperBodyIKChains = upperBodyIKChains;
        _networkConnection = networkConnection;
        _useHeuristikSolver = useHeuristikSolver;
        // Spine3 in our case
        _root = _upperBodyIKChains.Root;
        // Remove old root and replace with neck root
        Transform[] leftArmCCDChain = _upperBodyIKChains.LeftArmIKChain.Skip(1).ToArray();
        Transform[] rightArmCCDChain = _upperBodyIKChains.RightArmIKChain.Skip(1).ToArray();
        Transform[] headCCDChain = _upperBodyIKChains.HeadIKChain.Skip(1).ToArray();
        _simpleCCDLeftArm = new SimpleCCD(leftArmCCDChain, 0.001f, 500);
        _simpleCCDRightArm = new SimpleCCD(rightArmCCDChain, 0.001f, 500);
        _simpleCCDHead = new SimpleCCD(headCCDChain, 0.001f, 500);
        HandDistanceSMPL = 0f;
    }

    public void Update()
    {
        var headToLeftHandVector = (_upperBodyIKChains.LeftHand.position - _upperBodyIKChains.Head.position) /
                                 HandDistanceSMPL;
        var headToRightHandVector = (_upperBodyIKChains.RightHand.position - _upperBodyIKChains.Head.position) /
                                  HandDistanceSMPL;

        // Enforce quaternions to be on one specific hemisphere of the hypersphere (due to conjugate: q = -q)
        var leftHandRotation = QuaternionFunctions._ForceToOneSideOfHypersphere(_upperBodyIKChains.LeftHand.rotation);
        var rightHandRotation = QuaternionFunctions._ForceToOneSideOfHypersphere(_upperBodyIKChains.RightHand.rotation);
        var headRotation = QuaternionFunctions._ForceToOneSideOfHypersphere(_upperBodyIKChains.Head.rotation);

        // Send IK update request to network solver
        _networkConnection.SendMessage(BuildKinematicRequestPackage(headToLeftHandVector, headToRightHandVector, leftHandRotation,
            rightHandRotation, headRotation));

        // The return string has the following format:
        // r_spine1[x,y,z,w], r_neck[x,y,z,w], r_left_shoulder[x,y,z,w], r_left_arm[x,y,z,w], r_left_forearm[x,y,z,w],
        // r_right_shoulder[x,y,z,w], r_right_arm[x,y,z,w], r_right_forearm[x,y,z,w]
        var retString = _networkConnection.Listen();
        if (string.IsNullOrEmpty(retString))
        {
            Debug.LogWarning("Got invalid package for network kinematics.");
            return;
        }
        // All values are comma separated.
        var retValues = retString.Split(',');

        var quaternions = new Quaternion[retValues.Length / 4];
        for (int i = 0; i < retValues.Length / 4; i++)
        {
            if (string.IsNullOrEmpty(retValues[i]))
            {
                break;
            }

            // Get quaternion rotation of received package.
            float x = float.Parse(retValues[i * 4]);
            float y = float.Parse(retValues[i * 4 + 1]);
            float z = float.Parse(retValues[i * 4 + 2]);
            float w = float.Parse(retValues[i * 4 + 3]);
            quaternions[i] = new Quaternion(x, y, z, w);
        }

        // Update the IK chains with received rotation data form network IK solver
        _root.transform.rotation = quaternions[0];

        _upperBodyIKChains.LeftArmIKChain[4].rotation = leftHandRotation;
        _upperBodyIKChains.RightArmIKChain[4].rotation = rightHandRotation;
        _upperBodyIKChains.HeadIKChain[2].rotation = headRotation;

        _upperBodyIKChains.HeadIKChain[1].transform.localRotation = quaternions[1];
        _upperBodyIKChains.LeftArmIKChain[1].transform.localRotation = quaternions[2];
        _upperBodyIKChains.LeftArmIKChain[2].transform.localRotation = quaternions[3];
        _upperBodyIKChains.LeftArmIKChain[3].transform.localRotation = quaternions[4];
        _upperBodyIKChains.RightArmIKChain[1].transform.localRotation = quaternions[5];
        _upperBodyIKChains.RightArmIKChain[2].transform.localRotation = quaternions[6];
        _upperBodyIKChains.RightArmIKChain[3].transform.localRotation = quaternions[7];

        // Final IK solver step which pushes the end-effectors to the target positions.
        if (_useHeuristikSolver)
        {
            _simpleCCDLeftArm.Solve(_upperBodyIKChains.LeftHand.position);
            _simpleCCDRightArm.Solve(_upperBodyIKChains.RightHand.position);
        }
    }

    /// <summary>
    /// Build package to be send to the network solver client. Contains all input data for the solver.
    /// </summary>
    /// <param name="headToLeftHand">Vector from head to left hand joint.</param>
    /// <param name="headToRightHand">Vector from head to left hand joint.</param>
    /// <param name="leftHand">Left hand rotation as quaternion.</param>
    /// <param name="rightHand">Right hand rotation as quaternion.</param>
    /// <param name="head">Head rotation as quaternion.</param>
    /// <returns></returns>
    private static string BuildKinematicRequestPackage(Vector3 headToLeftHand, Vector3 headToRightHand,
        Quaternion leftHand, Quaternion rightHand, Quaternion head)
    {
        string msg =
            headToLeftHand.x + "," + headToLeftHand.y + "," + headToLeftHand.z + "," +
            headToRightHand.x + "," + headToRightHand.y + "," + headToRightHand.z + "," +
            head.x + "," + head.y + "," + head.z + "," + head.w + "," +
            leftHand.x + "," + leftHand.y + "," + leftHand.z + "," + leftHand.w + "," +
            rightHand.x + "," + rightHand.y + "," + rightHand.z + "," + rightHand.w;
        return msg;
    }
}