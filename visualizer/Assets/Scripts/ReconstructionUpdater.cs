﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReconstructionUpdater : MonoBehaviour
{
    [Header("Network")] [SerializeField] [Tooltip("The port to listen for incoming SMPL joint rotations.")]
    private int _listenPortSMPL;

    [SerializeField] [Tooltip("The port to send messages to rotation updater.")]
    private int _clientPortSMPL;

    [SerializeField] [Tooltip("The IP to send messages to rotation updater.")]
    private string _clientIPSMPL;

    [SerializeField] [Tooltip("The port to listen for incoming IK solver updates.")]
    private int _listenPortNetworkIK;

    [SerializeField] [Tooltip("The port to send data to the IK solver.")]
    private int _clientPortNetworkIK;

    [SerializeField] [Tooltip("The IP to send data to the IK solver.")]
    private string _clientIPNetworkIK;

    [Header("SMPL Tracking")] [SerializeField] [Tooltip("SMPL root joint.")]
    private Transform _smplRoot;

    [SerializeField] private SMPLBlendshapes _smplBlendshapes;

    [SerializeField] [Tooltip("Tracked HMD transform.")]
    private Transform _vrHead;

    [SerializeField] [Tooltip("Tracked left hand controller transform.")]
    private Transform _vrControllerLeft;

    [SerializeField] [Tooltip("Tracked right hand controller transform.")]
    private Transform _vrControllerRight;

    [SerializeField]
    [Tooltip("All SMPL bones from left wrist to neck. SMPL (root) -> L_Collar -> L_Shoulder -> L_Elbow -> L_Wrist.")]
    private Transform[] _smplChainLeftArm;

    [SerializeField]
    [Tooltip("All SMPL bones from right wrist to neck. SMPL (root) -> R_Collar -> R_Shoulder -> R_Elbow -> R_Wrist.")]
    private Transform[] _smplChainRightArm;

    [SerializeField]
    [Tooltip("All SMPL bones from neck to head. SMPL root transform as starting joint -> neck -> head.")]
    private Transform[] _smplChainHead;

    [Header("Inverse Kinematics")]
    [SerializeField]
    [Tooltip("The position of the SMPL head bone. Child of tracked HMD object.")]
    private Transform _ikTargetHead;

    [SerializeField] [Tooltip("The position of the SMPL left wrist bone. Child of tracked controller object.")]
    private Transform _ikTargetLeftHand;

    [SerializeField] [Tooltip("The position of the SMPL right wrist bone. Child of tracked controller object.")]
    private Transform _ikTargetRightHand;

    private UpperBodyIKSolver _networkIKSolver;
    private UpperBodyIKSolver _simpleCCDIKSolver;
    private Transform[] _bones;
    private SMPLModifyBones _modifyBones;
    private NetworkConnection _networkConnection;
    private Transform _smplHead;
    private Transform _smplHandLeft;
    private Transform _smplHandRight;
    private ReconstructionPackage _networkPackage;
    private FPSCalculator _fpsCalculator;
    private string _lastData = "";
    private IEnumerator _coroutine;

    private void Start()
    {
        _coroutine = _ReconstructionLoop();
        _networkConnection = new NetworkConnection(_listenPortSMPL, _clientIPSMPL, _clientPortSMPL, true);
        _networkPackage = new ReconstructionPackage();
        _modifyBones = _smplBlendshapes.GetModifiyBones();
        _bones = _modifyBones.getBones();
        _fpsCalculator = new FPSCalculator();
        _networkIKSolver = new UpperBodyIKSolver();
        var upperBodyIKChains = new UpperBodyIKSolver.UpperBodyIKChains
        {
            Head = _ikTargetHead,
            LeftHand = _ikTargetLeftHand,
            RightHand = _ikTargetRightHand,
            LeftArmIKChain = _smplChainLeftArm,
            RightArmIKChain = _smplChainRightArm,
            HeadIKChain = _smplChainHead,
            Root = _smplRoot
        };
        _networkIKSolver.NetworkSolverConnection =
            new NetworkConnection(_listenPortNetworkIK, _clientIPNetworkIK, _clientPortNetworkIK, false);
        _networkIKSolver.UseHeuristikSolverForNetworkSolver = false;
        _networkIKSolver.Init(UpperBodyIKSolver.SolverType.NETWORK, upperBodyIKChains);
        // Use CCD as final IK solver
        _simpleCCDIKSolver = new UpperBodyIKSolver();
        _simpleCCDIKSolver.Init(UpperBodyIKSolver.SolverType.CCD, upperBodyIKChains);

        // Get relevant bones for Tracking alignment
        _smplHandLeft = _smplChainLeftArm[_smplChainLeftArm.Length - 1];
        _smplHandRight = _smplChainRightArm[_smplChainRightArm.Length - 1];
        _smplHead = _smplChainHead[_smplChainHead.Length - 1];
    }

    /// <summary>
    /// Call this to start the reconstruction loop. 
    /// </summary>
    public void StartReconstruction()
    {
        StopCoroutine(_coroutine);
        Debug.Log("Start reconstruction update process");
        StartCoroutine(_coroutine);
    }

    /// <summary>
    /// Main update loop for full body reconstruction.
    /// </summary>
    private IEnumerator _ReconstructionLoop()
    {
        while (true)
        {
            var data = _networkConnection.GetData();
            if (string.IsNullOrEmpty(data))
            {
                Debug.LogWarning("No data received from reconstruction side. Make sure the PC is running correctly");
                continue;
            }
            if (data != _lastData)
            {
                _fpsCalculator.Update();
                _lastData = data;
            }

            Debug.Log("Current update rate: " + _fpsCalculator.CurrentFps());
            _networkPackage.Parse(data);
            if (_networkPackage.packageType == ReconstructionPackage.PackageType.Pose)
                _UpdatePose(_networkPackage.data);
            else
                Debug.LogError("Unknown package type: " + _networkPackage.packageType.ToString());

            // Solve upper body skeleton via network IK solver.
            _networkIKSolver.Solve();

            // Spine3 position based on IK calculation.
            var spine3Pos = _ikTargetHead.position + (_smplChainHead[0].position - _smplHead.position);
            _smplRoot.position = spine3Pos + (_smplRoot.position - _smplChainHead[0].position);
            _simpleCCDIKSolver.Solve();

            // Set rotation of head and both hands to ground truth VR-tracking rotations.
            _smplHead.rotation = _vrHead.rotation;
            _smplHandLeft.rotation = _vrControllerLeft.rotation * Quaternion.AngleAxis(90, Vector3.up);
            _smplHandRight.rotation = _vrControllerRight.rotation * Quaternion.AngleAxis(-90, Vector3.up);

            // Wait for next frame.
            yield return null;
        }
    }

    private void _UpdatePose(float[] pose)
    {
        var boneNameToJointIndex = _modifyBones.getB2J_indices();
        var boneNamePrefix = _modifyBones.getBoneNamePrefix();
        for (int i = 0; i < _bones.Length; i++)
        {
            int index;
            string boneName = _bones[i].name;

            // Remove f_avg/m_avg prefix
            boneName = boneName.Replace(boneNamePrefix, "");

            // Do not update root joint.
            if (boneName == "root")
            {
                continue;
            }

            // Check if current joint ID exists
            if (!boneNameToJointIndex.TryGetValue(boneName, out index)) continue;

            var poseIdx = index * 3;
            // Negate Y and Z to convert from right to left handed coordinate system
            var rot =  _AngleToQuaternion(pose[poseIdx], -pose[poseIdx + 1], -pose[poseIdx + 2]);
            // Pelvis joint is handled separately.
            if (boneName != "Pelvis") {		
                _bones [i].localRotation = rot;
            }
        }
    }

    /// <summary>
    /// Convert an axis angle representation with magnitude to quaternion
    /// Math from: http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToQuaternion/index.htm
    /// </summary>
    /// <param name="x">X-rotation of axis angle.</param>
    /// <param name="y">Y-rotation of axis angle.</param>
    /// <param name="z">Z-rotation of axis angle.</param>
    /// <returns></returns>
    private Quaternion _AngleToQuaternion(float x, float y, float z)
    {
        // Angle is the length of the vector
        float angle = Mathf.Sqrt(x * x + y * y + z * z);
        float x_norm = x / angle;
        float y_norm = y / angle;
        float z_norm = z / angle;
        float s = Mathf.Sin(angle / 2f);
        float qx = x_norm * s;
        float qy = y_norm * s;
        float qz = z_norm * s;
        float qw = Mathf.Cos(angle / 2f);
        return new Quaternion(qx, qy, qz, qw);
    }

    /// <summary>
    /// Update the arm distance from skeleton calibration.
    /// </summary>
    /// <param name="distance">Distance between both hands in meter.</param>
    public void SetArmDistanceNetworkSolver(float distance)
    {
        _networkIKSolver.SetArmDistanceNetworkSolver(distance);
    }
}