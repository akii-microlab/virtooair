﻿using UnityEngine;
using System;
using System.Net.Sockets;
using System.Net;
using System.Text;

/// <summary>
/// Network connection to _client. Listen for incoming _data.
/// </summary>
public class NetworkConnection
{
    private string _data;
    private UdpClient _client;
    private bool _autoUpdate;
    private int _listenPort;
    static readonly object _lock = new object();

    /// <summary>
    /// Connect to _client via UDP. 
    /// </summary>
    /// <param name="listenPort">Port to listen to.</param>
    /// <param name="clientIP">IP address of _client.</param>
    /// <param name="clientPort">Port of _client.</param>
    /// <param name="autoUpdate">Set to true to automatically check for new incoming packages. If false, call blocking function to get new _data.</param>
    public NetworkConnection(int listenPort, string clientIP, int clientPort, bool autoUpdate)
    {
        this._listenPort = listenPort;
        this._client = new UdpClient(listenPort);
        _client.Connect(clientIP, clientPort);
        this._data = "";
        this._autoUpdate = autoUpdate;

        // Set automatic receive callback if auto update is true.
        if (autoUpdate)
        {
            try
            {
                _client.BeginReceive(new AsyncCallback(AsyncListenCallback), null);
            }
            catch (Exception e)
            {
                Debug.LogError(e.ToString());
            }
        }
    }

    /// <summary>
    /// Blocking call. listen for new incoming package until timeout occurs.
    /// </summary>
    /// <param name="timeout">The max time to wait for new _data until a timeout occurs.</param>
    /// <returns></returns>
    public string Listen(float timeout = 0.1f)
    {
        if (_autoUpdate)
        {
            Debug.LogError("Listening is not possible because auto update is enabled.");
            return "";
        }

        var timeToWait = TimeSpan.FromSeconds(timeout);
        var asyncResult = _client.BeginReceive(null, null);
        asyncResult.AsyncWaitHandle.WaitOne(timeToWait);
        if (asyncResult.IsCompleted)
        {
            try
            {
                IPEndPoint remoteEP = null;
                byte[] receivedData = _client.EndReceive(asyncResult, ref remoteEP);
                _data = Encoding.ASCII.GetString(receivedData);
            }
            catch (Exception e)
            {
                Debug.LogError(e.ToString());
            }
        }
        else
        {
            Debug.LogWarning("Timeout while listening on Port: " + _listenPort);
        }

        return _data;
    }

    /// <summary>
    /// Listen for incoming packages callback
    /// </summary>
    private void AsyncListenCallback(IAsyncResult res)
    {
        var anyEndpoint = new IPEndPoint(IPAddress.Any, 0);
        var received = _client.Receive(ref anyEndpoint);
        lock (_lock)
        {
            _data = Encoding.ASCII.GetString(received);
        }

        _client.BeginReceive(new AsyncCallback(AsyncListenCallback), null);
    }

    /// <summary>
    /// Send new data to client.
    /// </summary>
    /// <param name="msg">Message to send to client.</param>
    public void SendMessage(string msg)
    {
        var bytes = Encoding.ASCII.GetBytes(msg);
        try
        {
            _client.Send(bytes, bytes.Length);
        }
        catch (Exception e)
        {
            Debug.LogError(e.ToString());
        }
    }

    /// <summary>
    /// Send new data to client.
    /// </summary>
    /// <param name="msg">Message to send to client.</param>
    public string GetData()
    {
        lock (_lock)
        {
            return _data;
        }
    }
}