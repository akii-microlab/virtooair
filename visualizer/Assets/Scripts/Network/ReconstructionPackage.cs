﻿using UnityEngine;

/// <summary>
/// Reconstruction package received from end-to-end reconstruction.
/// </summary>
public class ReconstructionPackage
{
    public enum PackageType { None, Pose, Shape, Texture };
    public PackageType packageType { get; set; }
    public float[] data { get; set; }

    /// <summary>
    /// Parse incoming package content.
    /// </summary>
    /// <param name="input">The incoming package from the reconstruction updater.</param>
    public void Parse(string input)
    {
        switch (input[0])
        {
            case 'p':
                packageType = PackageType.Pose;
                break;
            case 's':
                packageType = PackageType.Shape;
                break;
            case 't':
                packageType = PackageType.Texture;
                break;
            default:
                Debug.LogError("Unknown package type: " + input[0]);
                packageType = PackageType.None;
                break;
        }
        var dataValues = (input.Remove(0, 2)).Split(',');
        data = new float[dataValues.Length];
        for (int i = 0; i < dataValues.Length; i++)
        {
            if (!string.IsNullOrEmpty(dataValues[i]))
            {
                data[i] = float.Parse(dataValues[i]);
            }
        }
    }
}
