﻿using UnityEngine;

/// <summary>
/// Calibrate SMPL skeleton. Needs User in T-pose and scales model accordingly
/// </summary>
public class CalibrateSkeleton : MonoBehaviour {

	[SerializeField]
	private Transform _trackedHead;
	[SerializeField]
	private Transform _smplRoot;
	[SerializeField]
	private Transform _smplLeftHand;
	[SerializeField]
	private Transform _smplRightHand;
	[SerializeField]
	private Transform _smplHead;
	[SerializeField]
	private ReconstructionUpdater _reconstructionUpdater;
	[SerializeField]
	private Transform _controllerLeft;
	[SerializeField]
	private Transform _controllerRight;
	[SerializeField]
	private Transform _wristControllerLeft;
	[SerializeField]
	private Transform _wristControllerRight;

	private SteamVR_TrackedController _trackedControllerLeft;
	private SteamVR_TrackedController _trackedControllerRight;

	void Awake (){
		_trackedControllerLeft = _controllerLeft.GetComponent<SteamVR_TrackedController> ();
		_trackedControllerRight = _controllerRight.GetComponent<SteamVR_TrackedController> ();
	}

	void OnEnable () {
		if (_controllerLeft != null && _controllerRight != null) {
			_trackedControllerLeft.TriggerClicked += _Calibrate;
			_trackedControllerRight.TriggerClicked += _Calibrate;
		} else {
			Debug.LogError ("No controllers found!");
		}
	}

	void OnDisable () {
		_trackedControllerLeft.TriggerClicked -= _Calibrate ;
		_trackedControllerRight.TriggerClicked -= _Calibrate ;
	}

    /// <summary>
    /// Calibrate SMPL skeleton.
    /// </summary>
    /// <param name="sender">Event sender.</param>
    /// <param name="e">Additional click arguments.</param>
	private void _Calibrate(object sender, ClickedEventArgs e){
		Debug.Log("Start calibration...");
		float distanceBetweenController = Vector3.Distance (_wristControllerLeft.position, _wristControllerRight.position);
		float hmdHeadHeight = _trackedHead.position.y;
		float smplHandDistance = Vector3.Distance (_smplLeftHand.position, _smplRightHand.position);
		float smplHeight = _smplHead.position.y;
		float horizontalScale =  distanceBetweenController / smplHandDistance;
		float verticalScale = hmdHeadHeight / smplHeight;
		Debug.Log("Calibration hand distance = " + distanceBetweenController + ". Head Height = " + hmdHeadHeight);
		Debug.Log("SMPL wrist distance = " + smplHandDistance + ". SMPL height = " + smplHeight);
		_smplRoot.localScale = new Vector3(horizontalScale,verticalScale,1f);
		_reconstructionUpdater.SetArmDistanceNetworkSolver (distanceBetweenController);
		_reconstructionUpdater.StartReconstruction();
	}
}
