# VIRTOOAIR: VIrtual Reality TOOlbox for Avatar Intelligent Reconstruction

# Components
## System for VR motion reconstruction based on a VR tracking system and a single RGB camera
## Meta-learning system for adaptive unsupervised kinematics learning

**Armin Becher, Thomas Grauschopf, Cristian Axenie**

<img src="_images/architecture.png">

## Requirements
The whole project runs on two different computers because the end-to-end recovery framework only works with tensorflow and python 2.7. The part of VIRTOOAIR uses the popular game engine *Unity 3D* and python 3.6. Because *Unity 3D* is not officially supported for Linux, these parts of the whole system run on a Windows PC.

### Image Processing and End-to-End recovery 

- Linux OS tested on Ubuntu 16.0.4 LTS
- Open CV for python tested on version 3.4.0.12
- All requirements from the end-to-end recovery framework (see [link](https://github.com/akanazawa/hmr))


### Tracking Data Acquisition, Inverse Kinematics Regression, and Visualizer

- Windows 10
- Unity 2017.2.2f1 
- Python 3.6
- Keras
- Tensorflow 
- Pandas
- pyquaternion

## Installation

### End-to-End Recovery
The lower body of the avatar is reconstructed using a modified version of the end-to-end recovery framework by *Kanazawa et al.*. Please download and setup the project as described on the official [Github page](https://github.com/akanazawa/hmr).

Place the scripts provided in the [end-to-end-recovery folder](/end-to-end-recovery/) to the setup project. If you run the *virtooair.py* script you start the lower body reconstruction process which will use the attached camera to generate pose data which will be send to the Unity visualizer.

### IK Regression
The IK regression code was tested with **Python 3.6.** The two important scripts are *fit\_model.py* and *network\_solver.py*. Run the network solver script to use the provided pretrained model to send joint angle regression updates to the Unity 3D visualizer. 

Install all requirements via:

	pip install -r requirements.txt 

### Visualizer
The visualizer project uses the game development software *Unity 3D*. The project was tested with **Unity 2017.2.2f1**. Just open the provided Unity scene. Right now a male SMPL model is set as character. It can also be changed to the female version. Just add the female fbx model from the plugin folder to the scene and set the references in the reconstruction script analog to the male model. 

## Demo
If you correctly installed the components described above you can run the virtooair reconstruction code. Run the Visualizer and IK Regression model on your windows machine and the end-to-end recovery code on another Linux PC. If you setup the network correctly you will see that the lower body will be recovered with the end-to-end framework and the upper body uses the IK regression updater.

## Training
If you want to retrain the IK solver yourself open the [fit\_model.py](/ik-regression/fit_model.py) script and run it on the provided training data. The training data was generated as described in the paper. Please note that the validation loss will still be relative high. The reason for that is that the final upper body IK also uses a second CCD IK step which is not implemented in python, but C# and will be applied in *Unity 3D*. For more information refer to the paper. 

## License
This project itself is licensed under the MIT License. Please respect the license of the [SMPL model](http://smpl.is.tue.mpg.de/license) and the HTC Vive Unity plugins. See the [LICENSE](/LICENSE.md) file for details.